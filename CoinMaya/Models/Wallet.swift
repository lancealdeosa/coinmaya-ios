//
//  Wallet.swift
//  CoinMaya
//
//  Created by Lance on 10/11/21.
//

import Foundation

struct Wallet: Codable {
  let id: String
  let name: String
  let balance: Double
  let currency: Currency
  
  enum CodingKeys: String, CodingKey {
    case id
    case name = "wallet_name"
    case balance
    case currency
  }
}

struct GetWalletsResponse: Codable {
  let wallets: [Wallet]
}


