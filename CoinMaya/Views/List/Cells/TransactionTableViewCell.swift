//
//  TransactionTableViewCell.swift
//  CoinMaya
//
//  Created by Lance on 10/11/21.
//

import UIKit

protocol TransactionTableCellViewModel {
  var details: String { get }
  var amountString: String { get }
}

struct TransactionTableCellViewModelImp: TransactionTableCellViewModel {
  private let transaction: Transaction
  
  init(transaction: Transaction) {
    self.transaction = transaction
  }
  
  var details: String {
    switch transaction.entry {
    case .incoming:
      return "You've received payment"
    case .outgoing:
      return "\(transaction.sender) sent \(transaction.recipient)"
    }
  }
  var amountString: String { PriceFormatter.format(amount: transaction.amount, currency: transaction.currency) }
}

class TransactionTableViewCell: UITableViewCell {
  
  @IBOutlet weak var detailsLabel: UILabel!
  @IBOutlet weak var amountLabel: UILabel!
  
  func update(with model: TransactionTableCellViewModel) {
    detailsLabel.text = model.details
    amountLabel.text = model.amountString
  }
}

extension TransactionTableViewCell: NibLoadable {}
