//
//  WalletListView.swift
//  CoinMaya
//
//  Created by Lance on 10/10/21.
//

import UIKit

protocol ListViewModel {
  var isLoading: Bool { get }
  var isPaginated: Bool { get }
  var isForceErrorShown: Bool { get }
  
  var currentPage: Int { get }
  var contentCount: Int { get }
  
  func cellForRowAt(_ indexPath: IndexPath, forTableView tableView: UITableView) -> UITableViewCell
  func fetchContent(completion: @escaping (Result<Bool, Error>) -> Void)
  func toggleForceError(isOn: Bool)
}

extension ListViewModel {
  var isPaginated: Bool { false }
  var isForceErrorShown: Bool { false }
  var currentPage: Int { 0 }
  
  func toggleForceError(isOn: Bool) {}
}

class ListView: UIView {
  
  @IBOutlet private var contentView: UIView!
  @IBOutlet private weak var titleLabel: UILabel!
  @IBOutlet private weak var tableView: UITableView!
  @IBOutlet weak var forceErrorSwitch: UISwitch!
  
  private let viewModel: ListViewModel
  
  init(viewModel: ListViewModel) {
    self.viewModel = viewModel
    super.init(frame: .zero)
    setup()
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  func register<T: UITableViewCell>(type: T.Type) where T: NibLoadable  {
    tableView.register(type)
  }
  
  func updateTitle(_ title: String) {
    titleLabel.text = title
  }
  
  func fetchContent() {
    showLoadingView()
    tableView.removeErrorView()
    
    viewModel.fetchContent { [weak self] result in
      guard let self = self else { return }
      self.removeLoadingView()
      switch result {
      case .success:
        self.tableView.reloadData()
      case .failure(let error):
        self.tableView.showErrorView(with: error)
      }
    }
  }
  
  @IBAction private func toggleError(_ sender: UISwitch) {
    viewModel.toggleForceError(isOn: sender.isOn)
    fetchContent()
  }
}

extension ListView: UITableViewDataSource {
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return viewModel.contentCount
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    return viewModel.cellForRowAt(indexPath, forTableView: tableView)
  }
}

extension ListView: UITableViewDelegate {
  func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
    guard viewModel.isPaginated else { return }
    let nextPage = (indexPath.row + 1) / 10
    if nextPage >= viewModel.currentPage {
      fetchContent()
    }
  }
}

private extension ListView {
  private func setup() {
    setupNib()
    setupView()
  }
  
  private func setupNib() {
    Bundle.main.loadNibNamed("ListView", owner: self, options: nil)
    contentView.frame = bounds
    addSubview(contentView)
  }
  
  private func setupView() {
    tableView.dataSource = self
    tableView.delegate = self
    tableView.register(LoadingTableViewCell.self, forCellReuseIdentifier: LoadingTableViewCell.nibName)
    
    if viewModel.isForceErrorShown {
      forceErrorSwitch.isHidden = false
    }
  }
}

class LoadingTableViewCell: UITableViewCell {
  
  private lazy var label: UILabel = {
    let label = UILabel()
    label.text = "Loading"
    return label
  }()
  
  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setupLayout()
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  private func setupLayout() {
    contentView.addSubview(label)
    label.addInnerConstraint(.zero)
    NSLayoutConstraint.activate([ label.heightAnchor.constraint(equalToConstant: 50) ])
  }
}

extension LoadingTableViewCell: NibLoadable {}
