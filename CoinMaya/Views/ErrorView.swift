//
//  ErrorView.swift
//  CoinMaya
//
//  Created by Lance on 10/11/21.
//

import UIKit

class ErrorView: UIView {
  
  private lazy var titleLabel: UILabel = {
    let label = UILabel()
    label.text = "Error, please try again"
    return label
  }()
  
  init() {
    super.init(frame: .zero)
    setupView()
  }
  
  required init?(coder: NSCoder) { fatalError("init(coder:) has not been implemented") }
  
  func show(error: Error) {
    let message: String
    if let coinMayaError = error as? CoinMayaError {
      message = coinMayaError.message
    } else {
      message = CoinMayaError.generic.message
    }
    titleLabel.text = message
  }
}

private extension ErrorView {
  func setupView() {
    backgroundColor = .white
    addSubview(titleLabel)
    titleLabel.addInnerConstraint(.zero)
  }
}
