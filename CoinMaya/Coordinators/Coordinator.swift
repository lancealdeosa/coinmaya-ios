//
//  Coordinator.swift
//  CoinMaya
//
//  Created by Lance on 10/10/21.
//

import UIKit

protocol Coordinator {
  var navigationController: UINavigationController { get }
  var childCoordinators: [Coordinator] { get set }
  
  func start()
}
