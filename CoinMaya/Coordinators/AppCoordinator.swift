//
//  AppCoordinator.swift
//  CoinMaya
//
//  Created by Lance on 10/10/21.
//

import Foundation
import UIKit

class AppCoordinator: Coordinator {
  let navigationController: UINavigationController
  var childCoordinators: [Coordinator] = []
  
  init(navigationController: UINavigationController) {
    self.navigationController = navigationController
  }
  
  func start() {
    let homeViewController = HomeViewController()
    homeViewController.delegate = self
    navigationController.pushViewController(homeViewController, animated: false)
  }
}

extension AppCoordinator: HomeViewControllerDelegate {}
