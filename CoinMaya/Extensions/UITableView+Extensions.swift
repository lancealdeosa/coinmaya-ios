//
//  NibLoadable.swift
//  CoinMaya
//
//  Created by Lance on 10/11/21.
//

import UIKit

protocol NibLoadable: AnyObject {
  static var nibName: String { get }
}

extension NibLoadable where Self: UIView {
  static var nibName: String {
    return String(describing: self)
  }
}

protocol Identifiable: AnyObject {
  static var identifier: String { get }
}

extension Identifiable  {
  static var identifier: String {
    return String(describing: self)
  }
}

extension UITableView {
  func register<T: UITableViewCell>(_: T.Type) {
    register(T.self, forCellReuseIdentifier: T.identifier)
  }
  
  func register<T: UITableViewCell>(_: T.Type) where T: NibLoadable {
    let bundle = Bundle(for: T.self)
    let nib = UINib(nibName: T.nibName, bundle: bundle)
    register(nib, forCellReuseIdentifier: T.identifier)
  }
  
  func dequeueReusableCell<T: UITableViewCell>() -> T {
    guard let cell = dequeueReusableCell(withIdentifier: T.identifier) as? T else {
      fatalError("Could not dequeue cell with identifier: \(T.identifier)")
    }
    return cell
  }
  
  func dequeueReusableCell<T: UITableViewCell>(for indexPath: IndexPath) -> T {
    guard let cell = dequeueReusableCell(withIdentifier: T.identifier, for: indexPath) as? T else {
      fatalError("Could not dequeue cell with identifier: \(T.identifier)")
    }
    return cell
  }
  
  func cellForRow<T: UITableViewCell>(at indexPath: IndexPath) -> T {
    guard let cell = cellForRow(at: indexPath) as? T else {
      fatalError("Could not dequeue cell with identifier: \(T.identifier)")
    }
    return cell
  }
}

extension UITableViewCell: Identifiable {}
