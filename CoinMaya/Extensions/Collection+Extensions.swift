//
//  Collection+Extensions.swift
//  CoinMaya
//
//  Created by Lance on 10/11/21.
//

import Foundation

extension Collection {
  func distance(from startIndex: Index) -> Int {
    return distance(from: startIndex, to: self.endIndex)
  }
  
  func distance(to endIndex: Index) -> Int {
    return distance(from: self.startIndex, to: endIndex)
  }
  
  subscript(safe index: Index) -> Iterator.Element? {
    guard distance(to: index) >= 0 && distance(from: index) > 0 else {
      return nil
    }
    return self[index]
  }
}
