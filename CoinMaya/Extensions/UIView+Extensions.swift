//
//  UIView+Extensions.swift
//  CoinMaya
//
//  Created by Lance on 10/11/21.
//

import Foundation
import UIKit

extension UIView {
  var cornerRadius: CGFloat {
    get { layer.cornerRadius }
    set { layer.cornerRadius = newValue }
  }
  
  func addSubviews(_ views: UIView...) {
    views.forEach { addSubview($0) }
  }
  
  func addSubviews(_ views: [UIView]) {
    views.forEach { addSubview($0) }
  }
  
  @discardableResult
  func configureForAutoLayout() -> UIView {
    translatesAutoresizingMaskIntoConstraints = false
    return self
  }
  
  @discardableResult
  func addInnerConstraint(_ edges: UIEdgeInsets) -> [NSLayoutConstraint] {
    guard let superview = self.superview else { return [] }
    translatesAutoresizingMaskIntoConstraints = false
    
    let constraints: [NSLayoutConstraint] = [
      self.topAnchor.constraint(equalTo: superview.safeAreaLayoutGuide.topAnchor, constant: edges.top),
      self.leftAnchor.constraint(equalTo: superview.safeAreaLayoutGuide.leftAnchor, constant: edges.left),
      self.rightAnchor.constraint(equalTo: superview.safeAreaLayoutGuide.rightAnchor, constant: -edges.right),
      self.bottomAnchor.constraint(equalTo: superview.safeAreaLayoutGuide.bottomAnchor, constant: -edges.bottom)
   ]
    NSLayoutConstraint.activate(constraints)
    
    return constraints
  }
  
  func showErrorView(with error: Error) {
    removeErrorView()
    let errorView = ErrorView()
    
    addSubview(errorView)
    errorView.addInnerConstraint(.zero)
    errorView.show(error: error)
  }
  
  func removeErrorView() {
    guard let errorView = subviews.first(where: { $0 is ErrorView }) else { return }
    errorView.removeFromSuperview()
  }
  
  func showLoadingView() {
    removeLoadingView()
    let loadingView = LoadingView()
    
    addSubview(loadingView)
    loadingView.configureForAutoLayout()
    NSLayoutConstraint.activate([
      loadingView.centerXAnchor.constraint(equalTo: centerXAnchor),
      loadingView.centerYAnchor.constraint(equalTo: centerYAnchor),
      loadingView.heightAnchor.constraint(equalToConstant: 100),
      loadingView.widthAnchor.constraint(equalToConstant: 100),
    ])
    
    loadingView.startAnimating()
  }
  
  func removeLoadingView() {
    guard let loadingView = subviews.first(where: { $0 is LoadingView }) else { return }
    loadingView.removeFromSuperview()
  }
}
