//
//  TransactionListViewModel.swift
//  CoinMaya
//
//  Created by Lance on 10/11/21.
//

import Foundation
import UIKit

class TransactionListViewModelImp: ListViewModel {

  private let apiClient: APIClient
  
  private var transactions: [Transaction] = []
  private(set) var isLoading = false
  private(set) var currentPage = 0
  let isPaginated = true
  
  init(apiClient: APIClient = .shared) {
    self.apiClient = apiClient
  }
  
  var contentCount: Int {
    return transactions.count
  }
  
  func cellForRowAt(_ indexPath: IndexPath, forTableView tableView: UITableView) -> UITableViewCell {
    guard let transaction = transactions[safe: indexPath.row] else {
      return UITableViewCell()
    }
    let cell: TransactionTableViewCell = tableView.dequeueReusableCell(for: indexPath)
    let cellModel = TransactionTableCellViewModelImp(transaction: transaction)
    cell.update(with: cellModel)
    return cell
  }
  
  func fetchContent(completion: @escaping (Result<Bool, Error>) -> Void) {
    guard !isLoading else { return }
    isLoading = true
    currentPage += 1
    
    apiClient.getTransactions(page: currentPage) { [weak self] result in
      guard let self = self else { return }
      
      self.isLoading = false
      
      switch result {
      case .success(let newTransactions):
        guard !newTransactions.isEmpty else {
          completion(.success(false))
          return
        }
        self.transactions += newTransactions
        completion(.success(true))
      case .failure(let error):
        completion(.failure(error))
      }
    }
    
  }
}
