//
//  HomeViewController.swift
//  CoinMaya
//
//  Created by Lance on 10/10/21.
//

import UIKit

class HomeViewController: UIViewController {
  
  private lazy var walletListView: ListView = {
    let viewModel = WalletListViewModelImp()
    let view = ListView(viewModel: viewModel)
    
    view.configureForAutoLayout()
    view.register(type: WalletTableViewCell.self)
    view.updateTitle("My Wallets")
    
    return view
  }()
  
  private lazy var transactionListView: ListView = {
    let viewModel = TransactionListViewModelImp()
    let view = ListView(viewModel: viewModel)
    
    view.configureForAutoLayout()
    view.register(type: TransactionTableViewCell.self)
    view.updateTitle("History")
    
    return view
  }()
  
  weak var delegate: HomeViewControllerDelegate?
  
  init() {
    super.init(nibName: nil, bundle: nil)
  }
  
  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
  
  override func viewDidLoad() {
    super.viewDidLoad()
    setupLayout()
    setupView()
    fetchContent()
  }
  
  private func fetchContent() {
    walletListView.fetchContent()
    transactionListView.fetchContent()
  }
}

private extension HomeViewController {
  func setupLayout() {
    let listViews = [walletListView, transactionListView]
    view.addSubviews(listViews)
    
    var constraints: [NSLayoutConstraint] = [
      walletListView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 0),
      walletListView.bottomAnchor.constraint(equalTo: transactionListView.topAnchor, constant: 0),
      transactionListView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: 0),
      walletListView.heightAnchor.constraint(equalToConstant: 200.0)
    ]
    
    listViews.forEach {
      constraints.append(contentsOf: [
        $0.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 16),
        $0.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -16)
      ])
    }
    
    NSLayoutConstraint.activate(constraints)
  }
  
  func setupView() {
    view.backgroundColor = .white
  }
}

protocol HomeViewControllerDelegate: AnyObject {}
