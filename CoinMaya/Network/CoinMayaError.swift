//
//  CoinMayaError.swift
//  CoinMaya
//
//  Created by Lance on 10/11/21.
//

import Foundation

enum CoinMayaError: Error {
  case generic
  case networkFailure
  case custom(CoinMayaErrorResponse.CustomError)
}

extension CoinMayaError {
  var message: String {
    switch self {
    case .generic:
      return "Oops, something went wrong. Sorry."
    case .networkFailure:
      return "Oops, something's wrong with your internet connection."
    case .custom(let customError):
      return customError.message
    }
  }
}

struct CoinMayaErrorResponse: Codable {
  struct CustomError: Codable {
    let code: String
    let message: String
  }
  
  private let errors: [CustomError]
  var error: CustomError { errors.first! }
}
