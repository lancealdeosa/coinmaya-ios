//
//  CoinMayaAPI.swift
//  CoinMaya
//
//  Created by Lance on 10/11/21.
//

import Foundation
import Moya

enum CoinMayaEndpoint {
  case getWallets
  case getHistory(page: Int)
  
  case mockError
}

extension CoinMayaEndpoint {
  fileprivate enum Constants {
    static let defaultLimit: Int = 10
  }
}

extension CoinMayaEndpoint: TargetType {
  var baseURL: URL { URL(string: "https://6163d835b55edc00175c1b96.mockapi.io")! }
  
  var path: String {
    switch self {
    case .getWallets:
      return "/wallets"
    case .getHistory:
      return "/histories"
    case .mockError:
      return "/error"
    }
  }
  
  var method: Moya.Method { .get }
  
  var task: Task {
    switch self {
    case .getWallets, .mockError:
      return .requestPlain
    case .getHistory(let page):
      let params = [ "page": page, "limit": Constants.defaultLimit ]
      return .requestParameters(parameters: params, encoding: URLEncoding.default)
    }
  }
  
  var headers: [String : String]? { nil }
}
