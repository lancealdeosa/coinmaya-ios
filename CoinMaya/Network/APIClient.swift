//
//  APIClient.swift
//  CoinMaya
//
//  Created by Lance on 10/11/21.
//

import Foundation
import Alamofire
import Moya

typealias ResponseHandler = (Response) -> ()
typealias FailureHandler = (Error?) -> ()

class APIClient {
 
  static let shared = APIClient()
  
  private let provider: MoyaProvider<CoinMayaEndpoint> = {
    let configuration = URLSessionConfiguration.default
    configuration.timeoutIntervalForRequest = 20
    configuration.timeoutIntervalForResource = 20
    let session = Session.init(configuration: configuration)
    return MoyaProvider<CoinMayaEndpoint>(session: session)
  }()
  
  func getWallets(forceError: Bool = false, completion: @escaping (Result<[Wallet], Error>) -> Void) {
    request(target: .getWallets, type: GetWalletsResponse.self, forceError: forceError) { result in
      switch result {
      case .success(let response):
        completion(.success(response.wallets))
      case .failure(let error):
        completion(.failure(error))
      }
    }
  }
  
  func getTransactions(page: Int, completion: @escaping (Result<[Transaction], Error>) -> Void) {
    request(target: .getHistory(page: page), type: GetTransactionsResponse.self) { result in
      switch result {
      case .success(let response):
        completion(.success(response.transactions))
      case .failure(let error):
        completion(.failure(error))
      }
    }
  }
  
  private func request<T: Decodable>(target: CoinMayaEndpoint, type: T.Type, forceError: Bool, completion: @escaping (Result<T, Error>) -> Void) {
    if forceError {
      provider.request(.mockError) { [weak self] result in
        guard let self = self else { return }
        guard case .success(let response) = result,
          let errorResponse = self.parseData(response: response, type: CoinMayaErrorResponse.self)  else {
          completion(.failure(CoinMayaError.generic))
          return
        }
        completion(.failure(CoinMayaError.custom(errorResponse.error)))
      }
    } else {
      request(target: target, type: type, completion: completion)
    }
  }
  
  private func request<T: Decodable>(target: CoinMayaEndpoint, type: T.Type, completion: @escaping (Result<T, Error>) -> Void) {
    provider.request(target) { [weak self] result in
      DispatchQueue.main.async {
        guard let self = self else { return }
        switch result {
        case .success(let response):
          switch response.statusCode {
          case 200:
            guard let responseData = self.parseData(response: response, type: type) else {
              completion(.failure(CoinMayaError.networkFailure))
              return
            }
            completion(.success(responseData))
          case 429:
            guard let errorResponse = self.parseData(response: response, type: CoinMayaErrorResponse.self) else {
              completion(.failure(CoinMayaError.networkFailure))
              return
            }
            completion(.failure(CoinMayaError.custom(errorResponse.error)))
          default:
            completion(.failure(CoinMayaError.networkFailure))
          }
        case .failure(let error):
          completion(.failure(error))
        }
      }
    }
  }
  
  private func parseData<U: Decodable>(response: Response, type: U.Type) -> U? {
    guard let data = try? JSONDecoder().decode(type, from: response.data) else {
      return nil
    }
    return data
  }

}
