//
//  SceneDelegate.swift
//  CoinMaya
//
//  Created by Lance on 10/10/21.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

  var window: UIWindow?
  var coordinator: AppCoordinator?

  func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
    guard let windowScene = scene as? UIWindowScene else { return }
    
    let navigationController = UINavigationController()
    coordinator = AppCoordinator(navigationController: navigationController)
    
    let window = UIWindow(windowScene: windowScene)
    window.rootViewController = HomeViewController()
    window.makeKeyAndVisible()
    self.window = window
    
    coordinator?.start()
  }
}

