//
//  TransactionTableCellViewModelTests.swift
//  CoinMayaTests
//
//  Created by Lance on 10/12/21.
//

import XCTest
@testable import CoinMaya

class TransactionTableCellViewModelTests: XCTestCase {
  
  let mockIncomingTransaction = Transaction(id: "abcd", entry: .incoming, amount: 200, currency: .peso, sender: "BPI", recipient: "Lance")
  let mockOutgoingTransaction = Transaction(id: "abcd", entry: .outgoing, amount: 10, currency: .dollar, sender: "Lance", recipient: "Lou")
  
  var incomingViewModel: TransactionTableCellViewModelImp!
  var outgoingViewModel: TransactionTableCellViewModelImp!
  
  override func setUp() {
    super.setUp()
    incomingViewModel = TransactionTableCellViewModelImp(transaction: mockIncomingTransaction)
    outgoingViewModel = TransactionTableCellViewModelImp(transaction: mockOutgoingTransaction)
  }
  
  func testDetails() {
    XCTAssertEqual(incomingViewModel.details, "You've received payment")
    XCTAssertEqual(outgoingViewModel.details, "Lance sent Lou")
  }
  
  func testAmountString() {
    XCTAssertEqual(incomingViewModel.amountString, "200.00 PHP")
    XCTAssertEqual(outgoingViewModel.amountString, "10.00 USD")
  }
}
