//
//  WalletTableCellViewModelTests.swift
//  CoinMayaTests
//
//  Created by Lance on 10/12/21.
//

import XCTest
@testable import CoinMaya

class WalletTableCellViewModelTests: XCTestCase {
  
  let pesoWallet = Wallet(id: "1234", name: "🇵🇭", balance: 2000, currency: .peso)
  let dollarWallet = Wallet(id: "1234", name: "🇺🇸", balance: 350, currency: .dollar)
  
  var pesoViewModel: WalletTableCellViewModelImp!
  var dollarViewModel: WalletTableCellViewModelImp!
  
  override func setUp() {
    super.setUp()
    pesoViewModel = WalletTableCellViewModelImp(wallet: pesoWallet)
    dollarViewModel = WalletTableCellViewModelImp(wallet: dollarWallet)
  }
  
  func testName() {
    XCTAssertEqual(pesoViewModel.name, "🇵🇭")
    XCTAssertEqual(dollarViewModel.name, "🇺🇸")
  }
  
  func testBalance() {
    XCTAssertEqual(pesoViewModel.balanceString, "2000.00")
    XCTAssertEqual(dollarViewModel.balanceString, "350.00")
  }
}
